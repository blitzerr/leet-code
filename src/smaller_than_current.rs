pub fn smaller_numbers_than_current(nums: Vec<i32>) -> Vec<i32> {
    nums.iter()
        .map(|x| nums.iter().filter(|y| *y < x).count() as i32)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::smaller_numbers_than_current;

    #[test]
    fn t() {
        let x = vec![8, 1, 2, 2, 3];
        assert_eq!(vec![4, 0, 1, 1, 3], smaller_numbers_than_current(x));
    }
}

pub mod good_pairs_1512;
pub mod jewels_and_stones_771;
pub mod smaller_than_current;
pub mod tribonacci_number;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}

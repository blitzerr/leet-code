//! https://leetcode.com/problems/jewels-and-stones/
//! You're given strings jewels representing the
//! types of stones that are jewels, and stones
//! representing the stones you have. Each character
//! in stones is a type of stone you have. You want
//! to know how many of the stones you have are also jewels.

//! Letters are case sensitive, so "a" is considered a
//! different type of stone from "A".
//!
//! Example 1:
//! Input: jewels = "aA", stones = "aAAbbbb"
//! Output: 3
//!
//! Input: jewels = "z", stones = "ZZ"
//! Output: 0
//!
//! Constriant:
//! - 1 <= jewels.length, stones.length <= 50
//! - jewels and stones consist of only English letters.
//! - All the characters of jewels are unique.

use std::collections::HashSet;

pub fn num_jewels_in_stones(jewels: String, stones: String) -> i32 {
    let jewels = jewels.chars().collect::<HashSet<_>>();
    stones
        .chars()
        .map(|s| if jewels.contains(&s) { 1 } else { 0 })
        .sum()
}

pub fn num_jewels_in_stones_faster_hashset(jewels: String, stones: String) -> i32 {
    let jewels: HashSet<char> = HashSet::from_iter(jewels.chars());
    stones
        .chars()
        // This is a genuine method but for each character we have to compute the hash
        // which is expensive
        .filter_map(|s| if jewels.contains(&s) { Some(1) } else { None })
        .sum()
}

// credits: https://leetcode.com/problems/jewels-and-stones/submissions/887716339/
pub fn num_of_jewels_using_english_letter(jewels: String, stones: String) -> i32 {
    let mut jewel_vec = [false; 256];
    jewels
        .as_bytes()
        .iter()
        .for_each(|c| jewel_vec[*c as usize] = true);
    stones
        .as_bytes()
        .iter()
        // This is a genuine method but for each character we have to compute the hash
        // which is expensive
        .filter_map(|s| {
            if jewel_vec[*s as usize] {
                Some(1)
            } else {
                None
            }
        })
        .sum()
}

pub fn num_of_jewels_iterators_are_slow(jewels: String, stones: String) -> i32 {
    let mut jewel_vec = [false; 256];
    jewels
        .as_bytes()
        .iter()
        .for_each(|c| jewel_vec[*c as usize] = true);

    let mut count = 0;
    stones.as_bytes().iter().for_each(|stone| {
        if jewel_vec[*stone as usize] {
            count += 1;
        }
    });
    count
}

#[cfg(test)]
mod tests {
    use super::num_jewels_in_stones;

    #[test]
    fn some_jewels() {
        assert_eq!(
            3,
            num_jewels_in_stones("aA".to_string(), "aAAbbbb".to_string())
        );
    }
    #[test]
    fn no_jewels() {
        assert_eq!(0, num_jewels_in_stones("z".to_string(), "ZZ".to_string()));
    }
}

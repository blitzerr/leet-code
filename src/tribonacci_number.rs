//! The Tribonacci sequence Tn is defined as follows:
//! T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
//!
//! Input: n = 4
//! Output: 4
//! Explanation:
//! T_3 = 0 + 1 + 1 = 2
//! T_4 = 1 + 1 + 2 = 4
//!
//! Input: n = 25
//! Output: 1389537

pub fn tribonacci(n: i32) -> i32 {
    let mut t0 = 0;
    let mut t1 = 1;
    let mut t2 = 1;

    let mut t_n;
    let mut i = 2;

    match n {
        0 => t0,
        1 => t1,
        2 => t2,
        _ => loop {
            t_n = t0 + t1 + t2;
            i += 1;
            if n == i {
                break t_n;
            } else {
                t0 = t1;
                t1 = t2;
                t2 = t_n;
            }
        },
    }
}

/// Credits: https://leetcode.com/problems/n-th-tribonacci-number/submissions/887805623/
pub fn slick(n: i32) -> i32 {
    (0..n).fold((0, 1, 1), |(a, b, c), _| (a + b + c, a, b)).0
}

#[cfg(test)]
mod tests {
    use super::tribonacci;

    #[test]
    fn t_4() {
        assert_eq!(4, tribonacci(4));
    }

    #[test]
    fn t_25() {
        assert_eq!(1389537, tribonacci(25));
    }

    #[test]
    fn t_0() {
        assert_eq!(0, tribonacci(0));
    }
    #[test]
    fn t_1() {
        assert_eq!(1, tribonacci(1));
    }
}

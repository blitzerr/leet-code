//!  Number of Good Pairs
//!
//! Given an array of integers nums, return the number of good pairs.
//! A pair `(i, j)` is called good if `nums[i] == nums[j]` and `i < j`.
//!
//! Input: nums = [1,2,3,1,1,3]
//! Output: 4
//! Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
//!
//! Input: nums = [1,1,1,1]
//! Output: 6
//! Explanation: Each pair in the array are good.
//!
//! Input: nums = [1,2,3]
//! Output: 0
//!
//! Constraints:
//! 1 <= nums.length <= 100
//! 1 <= nums[i] <= 100

use std::collections::HashMap;

/// Given a number, it gives you how many pairs can be constructed from them.
fn pairs(num: i32) -> i32 {
    (num * (num - 1)) / 2
}

/// This is the intuitive solution where we use a map to keep track of
/// unique elements seen so far, and when we see another one of them we
/// add it to the vector. This provides a map of vector, where given a
/// number we get all the indices of the array which matches this number.
/// for each entry in the map, the good pair is the all possible ways of
/// choosing 2 of elements which is given by the pairs function.
/// And then all possible ways of across all the elements of the array
/// is the sum of such calculations for each entry in the map.
pub fn num_identical_pairs(nums: Vec<i32>) -> i32 {
    let map: HashMap<i32, Vec<usize>> = HashMap::default();

    let processed = nums
        .into_iter()
        .enumerate()
        .fold(map, |mut acc, (i, elem)| {
            let map_entry = dbg!(acc.entry(elem).or_default());
            map_entry.push(i);
            acc
        });
    println!("{processed:?}");
    processed.values().map(|val| pairs(val.len() as i32)).sum()
}

/// So there are certain optimizations that can be done. So for the
/// the number of matching elements, we only call the vectors' len/
/// Therefore, storing all of them in a vector is an overkill. We could
/// just use a counter. Let's do that.

pub fn num_identical_pairs_with_counter(nums: Vec<i32>) -> i32 {
    let processed: HashMap<i32, i32, _> =
        nums.into_iter()
            .fold(HashMap::default(), |mut map: HashMap<i32, i32>, i| {
                *map.entry(i).or_insert(0) += 1;
                map
            });
    processed.values().map(|ct| pairs(*ct)).sum()
}

/// Our version `num_identical_pairs_with_counter` takes less memory than
/// the previous version, as here we do not use a vec. But there is one
/// more optimization we can do.
///
/// we have two loops. we loop over the array of numbers and construct
/// the hash-map and then we iterate over the elements of the hashmap.
/// The question is can we do away with the second loop.
///
/// At first sigh it lools like we cannot do it because how can
/// we find number of good-pairs when we do not know the count
/// of elements in the group.But this changes with the introduction
/// of one mathematical fact that the formula for
/// (n C 2) = (n * (n - 1)) / 2   is also the formula for the sum
/// of n numbers.
/// This changes everything. Now instead for fully realizing the HashMap,
/// we can keep computing the result as the numbers come and this way we
/// can avoid the second pass over the map.
/// `processed.values().map(|ct| pairs(*ct)).sum()`
///
/// let's see why the mathematical formula is the same.
/// Say you want N people in a group to shake hands with each other and
/// you want to count the number of hand-shakes. Clearly, the formula is
/// is N choose 2. We do not doubt that. What we want to understand is why
/// is this the sum of first N numbers. For this, lets arrange these people
/// in a row, and the person on the left most end, gets up and walks to our
/// right shaking hands with every one along the way and when they have done
/// it with every one, they leave the room. They shook hands with everyone
/// in the room but themselves and therefore, there were N-1 handshakes.
/// The next person on the extreme right gets up and start doing the same.
/// Now this person shakes hands with N-2 people as the first person already
/// left the room. This goes on until the last but one person only has one
/// remaining person to shake hands with. Therefore the total handshakes
/// that happended in this room were:
/// N-1 + N-2 + N-3 + ... + 1.
///
/// Now let's have another take at this.

pub fn num_identical_pairs_optimized(nums: Vec<i32>) -> i32 {
    nums.into_iter()
        .fold(
            (HashMap::default(), 0),
            |(mut elements_to_repetition_map, pairs_so_far): (HashMap<i32, i32>, i32), elem| {
                let group_size = elements_to_repetition_map.entry(elem).or_default();
                // See that we are updating the pairs based on the last count
                // and not after incrementing current_val. This is because,
                // if you remenber, each person shakes hands 1 less time than
                // the group size.
                let new_pairs_so_far = pairs_so_far + *group_size;
                *group_size += 1;
                (elements_to_repetition_map, new_pairs_so_far)
            },
        )
        .1
}

#[cfg(test)]
mod tests {
    use crate::good_pairs_1512::{num_identical_pairs_optimized, num_identical_pairs_with_counter};

    use super::num_identical_pairs;

    #[test]
    fn when_all_same() {
        let nums = vec![1, 1, 1, 1];
        assert_eq!(6, num_identical_pairs(nums.clone()));
        assert_eq!(6, num_identical_pairs_with_counter(nums.clone()));
        assert_eq!(6, num_identical_pairs_optimized(nums));
    }

    #[test]
    fn when_all_different() {
        let nums = vec![1, 2, 3];
        assert_eq!(0, num_identical_pairs(nums.clone()));
        assert_eq!(0, num_identical_pairs_with_counter(nums.clone()));
        assert_eq!(0, num_identical_pairs_optimized(nums));
    }
}
